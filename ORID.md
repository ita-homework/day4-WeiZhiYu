# O

a. Code Review
- observer  pattern
- auto override equals and hashcode
- check if contains when List type variable add element

b. TDD(Test Driven Development)
- steps: 
- 1. Write a test
- 2. Make the code work
- 3. Eliminate redundant
- test struct: Given...When...Then
- test namning: should...when...given
- benefits:
- 1. Simple design
- 2. Test as Document
- 3. Quick feeback
- 4. Safety net

# R
I was excited!.
# I
TDD makes me feel more confident in my code.
# D
In the future, I will write tests before I write code.