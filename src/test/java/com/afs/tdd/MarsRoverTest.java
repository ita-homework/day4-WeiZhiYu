package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MarsRoverTest {
    @Test
    void should_change_to_0_1_N_when_executeCommand_given_location_0_0_N_command_M() {
        // Given
        Location initialLocation = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.MOVE;
        // When
        Location locationAfterMove = marsRover.executeCommand(command);
        // Then
        Assertions.assertEquals(new Location(0, 1, Direction.NORTH), locationAfterMove);
    }

    @Test
    void should_change_to_1_0_E_when_executeCommand_given_location_0_0_E_command_M() {
        // Given
        Location initialLocation = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.MOVE;
        // When
        Location locationAfterMove = marsRover.executeCommand(command);
        // Then
        Assertions.assertEquals(new Location(1, 0, Direction.EAST), locationAfterMove);
    }

    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_1_S_command_M() {
        // Given
        Location initialLocation = new Location(0, 1, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.MOVE;
        // When
        Location locationAfterMove = marsRover.executeCommand(command);
        // Then
        Assertions.assertEquals(new Location(0, 0, Direction.SOUTH), locationAfterMove);
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_1_0_W_command_M() {
        // Given
        Location initialLocation = new Location(1, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.MOVE;
        // When
        Location locationAfterMove = marsRover.executeCommand(command);
        // Then
        Assertions.assertEquals(new Location(0, 0, Direction.WEST), locationAfterMove);
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_0_0_N_command_L() {
        // Given
        Location initialLocation = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_LEFT;
        // When
        Location locationTurnLeft = marsRover.executeCommand(command);
        // Then
        Assertions.assertEquals(new Location(0, 0, Direction.WEST), locationTurnLeft);
    }

    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_0_W_command_L() {
        // Given
        Location initialLocation = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_LEFT;
        // When
        Location locationTurnLeft = marsRover.executeCommand(command);
        // Then
        Assertions.assertEquals(new Location(0, 0, Direction.SOUTH), locationTurnLeft);
    }

    @Test
    void should_change_to_0_0_E_when_executeCommand_given_location_0_0_S_command_L() {
        // Given
        Location initialLocation = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_LEFT;
        // When
        Location locationTurnLeft = marsRover.executeCommand(command);
        // Then
        Assertions.assertEquals(new Location(0, 0, Direction.EAST), locationTurnLeft);
    }

    @Test
    void should_change_to_0_0_N_when_executeCommand_given_location_0_0_E_command_L() {
        // Given
        Location initialLocation = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_LEFT;
        // When
        Location locationTurnLeft = marsRover.executeCommand(command);
        // Then
        Assertions.assertEquals(new Location(0, 0, Direction.NORTH), locationTurnLeft);
    }

    @Test
    void should_change_to_0_0_E_when_executeCommand_given_location_0_0_N_command_R() {
        // Given
        Location initialLocation = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_RIGHT;
        // When
        Location locationTurnLeft = marsRover.executeCommand(command);
        // Then
        Assertions.assertEquals(new Location(0, 0, Direction.EAST), locationTurnLeft);
    }

    @Test
    void should_change_to_0_0_S_when_executeCommand_given_location_0_0_E_command_R() {
        // Given
        Location initialLocation = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_RIGHT;
        // When
        Location locationTurnLeft = marsRover.executeCommand(command);
        // Then
        Assertions.assertEquals(new Location(0, 0, Direction.SOUTH), locationTurnLeft);
    }

    @Test
    void should_change_to_0_0_W_when_executeCommand_given_location_0_0_S_command_R() {
        // Given
        Location initialLocation = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_RIGHT;
        // When
        Location locationTurnLeft = marsRover.executeCommand(command);
        // Then
        Assertions.assertEquals(new Location(0, 0, Direction.WEST), locationTurnLeft);
    }

    @Test
    void should_change_to_0_0_N_when_executeCommand_given_location_0_0_W_command_R() {
        // Given
        Location initialLocation = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(initialLocation);
        Command command = Command.TURN_RIGHT;
        // When
        Location locationTurnLeft = marsRover.executeCommand(command);
        // Then
        Assertions.assertEquals(new Location(0, 0, Direction.NORTH), locationTurnLeft);
    }

    @Test
    void should_change_to_1_2_N_when_executeCommand_given_location_0_0_N_command_MRMLM() {
        // Given
        Location initialLocation = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(initialLocation);
        List<Command> commands = new ArrayList<>();
        commands.add(Command.MOVE);
        commands.add(Command.TURN_RIGHT);
        commands.add(Command.MOVE);
        commands.add(Command.TURN_LEFT);
        commands.add(Command.MOVE);
        // When
        Location locationExecuteBatchCommands = marsRover.executeBatchCommands(commands);
        // Then
        Assertions.assertEquals(new Location(1, 2, Direction.NORTH), locationExecuteBatchCommands);
    }
}
