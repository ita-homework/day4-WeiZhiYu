package com.afs.tdd;

import java.util.Arrays;
import java.util.List;

public class MarsRover {

    private static final List<Direction> directions = Arrays.asList(
            Direction.NORTH,
            Direction.EAST,
            Direction.SOUTH,
            Direction.WEST
    );

    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public Location executeCommand(Command command) {
        if (command.equals(Command.MOVE)) {
            move(this.location.getDirection());
        }
        if (command.equals(Command.TURN_LEFT)) {
            turnLeft();
        }
        if (command.equals(Command.TURN_RIGHT)) {
            turnRight();
        }
        return this.location;
    }

    private void turnRight() {
        this.location.setDirection(
                directions.get(
                        (directions.indexOf(this.location.getDirection()) + 1) % 4
                )
        );
    }

    private void turnLeft() {
        this.location.setDirection(
                directions.get(
                        (directions.indexOf(this.location.getDirection()) + 3) % 4
                )
        );
    }

    private void move(Direction direction) {
        if (direction.equals(Direction.NORTH)) {
            this.location.setCoordinateY(this.location.getCoordinateY() + 1);
        }
        if (direction.equals(Direction.EAST)) {
            this.location.setCoordinateX(this.location.getCoordinateX() + 1);
        }
        if (direction.equals(Direction.SOUTH)) {
            this.location.setCoordinateY(this.location.getCoordinateY() - 1);
        }
        if (direction.equals(Direction.WEST)) {
            this.location.setCoordinateX(this.location.getCoordinateX() - 1);
        }
    }

    public Location executeBatchCommands(List<Command> commands) {
        commands.forEach(this::executeCommand);
        return this.location;
    }
}
