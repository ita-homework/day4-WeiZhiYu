package com.afs.tdd;

public enum Direction {
    EAST, SOUTH, WEST, NORTH
}
