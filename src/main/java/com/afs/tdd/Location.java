package com.afs.tdd;

import java.util.Objects;

public class Location {
    private Integer coordinateX;
    private Integer coordinateY;
    private Direction direction;

    public Location(Integer coordinateX, Integer coordinateY, Direction direction) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
        this.direction = direction;
    }

    public Integer getCoordinateX() {
        return coordinateX;
    }

    public void setCoordinateX(Integer coordinateX) {
        this.coordinateX = coordinateX;
    }

    public Integer getCoordinateY() {
        return coordinateY;
    }

    public void setCoordinateY(Integer coordinateY) {
        this.coordinateY = coordinateY;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equals(coordinateX, location.coordinateX) && Objects.equals(coordinateY, location.coordinateY) && direction == location.direction;
    }

    @Override
    public int hashCode() {
        return Objects.hash(coordinateX, coordinateY, direction);
    }
}
